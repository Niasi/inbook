class SessionsController < ApplicationController
  def new

  end

  def create
    user = User.find_by(email: params[:session][:email])
    if user && user.authenticate(params[:session][:password])
      log_in(user)
      redirect_to user
    else
      flash.now[:danger] = 'Combinação de e-mail e senha inválida.'
      render 'new'
    end
  end
  
  def destroy
    log_out
    redirect_to root_path
  end

  private

    def logged_user_access
      if logged_in?
        redirect_to current_user
      end
    end
    
    def not_logged_user_access
      if !logged_in?
        redirect_to login_path
      end
    end  
end
