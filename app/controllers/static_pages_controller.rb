class StaticPagesController < ApplicationController
    before_action :logged_user_access, only: [:home]
    
    def home
        
    end
    
    private

        def logged_user_access
            if logged_in?
                redirect_to current_user
            end
        end
end
